// 2.1 Convierte la siguiente promesa para esperar a ejecutar el console usando 
// async-await.
// const runTimeOut = () => {
//     const promise = new Promise((resolve) => {
//         setTimeout(function () {
//             resolve();
//         }, 2000);
//     })

//     promise.then(() => {console.log('Time out!')})
// };

const runTimeOut = async() => {
    return await setTimeout(console.log('Time out!'),2000);
                     
}
runTimeOut();

// 2.2 Convierte la siguiente función con un fetch utilizando async-await. 
// Recuerda que para usar .fetch() tendrás que probar el ejercicio en el navegador;
// function getCharacters () {
//     fetch('https://rickandmortyapi.com/api/character').then(res => res.json()).then(characters => console.log(characters));
// }

// getCharacters();
window.onload=function(){
    init();
}
const init = async () => {

    const result =await getCharacters();
    console.log(result);
    
}
    
const getCharacters = async() => {
    const result = await fetch('https://rickandmortyapi.com/api/character');
    const resultToJson = await result.json();
    return resultToJson;
}