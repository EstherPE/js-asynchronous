// 1.1 Utiliza esta url de la api Agify 'https://api.agify.io?name=michael' para 
// hacer un .fetch() y recibir los datos que devuelve. Imprimelo mediante un 
// console.log(). Para ello, es necesario que crees un .html y un .js.

// window.onload=function(){
//     init();
// }

// const init = async () => {
//     console.log('init');
//     const result =await getInfo();
//     console.log(result);

// }

// const getInfo = async() => {
//     console.log('estoy en getInfo');
//     const result = await fetch('https://api.agify.io?name=michael');
//     const resultToJson = await result.json();
//     return resultToJson;
// }

// 2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando 
// fetch() para hacer una consulta a la api cuando se haga click en el botón, 
// pasando como parametro de la api, el valor del input.

const baseUrl = 'https://api.nationalize.io?name={aqui va el nombre}';

let consultButton = document.querySelector('button');

const consultar = async() => {
    let input = document.querySelector('input');
    let result= await getInfo(input.value);
    console.log(result);
    printName(result);
}

consultButton.addEventListener('click',consultar);

const getInfo = async(name) => {
    console.log('estoy en getInfo');
    const result = await fetch(`https://api.agify.io?name=${name}`);
    const resultToJson = await result.json();
    return resultToJson;
}




// 2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición 
// a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
// EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser 
// de MZ.

// 2.4 En base al ejercicio anterior, crea un botón con el texto 'X' para cada uno 
// de los p que hayas insertado y que si el usuario hace click en este botón 
// eliminemos el parrafo asociado.


const printName = (result) => {
    let newP = document.createElement('p');
    newP.textContent='El nombre '+result.name+ ' tiene '+result.age+ ' años.';
    let newButton = document.createElement('button');
    newButton.textContent='X';
    newButton.addEventListener('click', function deleteItem () {
        
        this.parentNode.remove();
    });
    document.body.appendChild(newP);
    newP.appendChild(newButton);

}

